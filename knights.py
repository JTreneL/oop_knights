from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")

# HP pool, where object taking basic hp without gear. 
hppool = [200,250,300,350]

# Gear
heads = {
  "Templarhelm": 200,
  "Helmoffallensoldier": 100,
  "Inquisitorhelm": 150,
  "Slavemask": 50
}

shoulder = {
  "Shouldersoflight": 160,
  "Shoulderxcross": 100,
  "Shoulderofshadows": 120,
  "Shoulderofbigvalley": 14
}

chest = {
  "Templarchest": 200,
  "Fallensoldierchest": 220,
  "Inquisitorchest": 250,
  "chestoftrug": 75
}

cloak = {
  "Cloakoftemplar": 150,
  "Cloakoffallensoldier": 100,
  "Cloakofshadows": 120,
  "Cloakoforder": 135
}

boots = {
  "Bootsheavy": 80,
  "Bootsxcross": 95,
  "Bootsoforder": 75,
  "Bootofslave": 68
}


# CLS knight
class knight:
  
  def __init__(self,name,head,shoulder,chest,cloak,boots,hp,dmg):
    self.name = name
    self.head = head
    self.shoulder = shoulder
    self.chest = chest
    self.cloak = cloak
    self.boots = boots
    self.hp = int(hp) + self.head + self.shoulder + self.chest + self.cloak + self.boots
    self.dmg = int(dmg)
  
  def introduction(self):
    print(f"I am knight {self.name}, nice to meet you.")
    print(f"My hp is {self.hp}.")
  
  def attack(self):
    print(self.dmg)

  def deffend(self):
    pass

 
# Knights
Rodriguez = knight("Rodriguez", 
                  heads.get("Templarhelm"),
                  shoulder.get("Shoulderxcross"),
                  chest.get("Templarchest"),
                  cloak.get("Cloakoftemplar"),
                  boots.get("Bootsheavy"),
                  hppool[0],150)

Hernan = knight("Hernan",
                heads.get("Templarhelm"),
                shoulder.get("Shoulderxcross"),
                chest.get("Templarchest"),
                cloak.get("Cloakoftemplar"),
                boots.get("Bootsheavy"),
                hppool[0],150)

Johann = knight("Johann",
                heads.get("Templarhelm"),
                shoulder.get("Shoulderxcross"),
                chest.get("Templarchest"),
                cloak.get("Cloakoftemplar"),
                boots.get("Bootsheavy"),
                hppool[0],150)

Ivo =  knight("Ivo",
              heads.get("Templarhelm"),
              shoulder.get("Shoulderxcross"),
              chest.get("Templarchest"),
              cloak.get("Cloakoftemplar"),
              boots.get("Bootsheavy"),
              hppool[0],150)

Sendal = knight("Sendal",
                heads.get("Templarhelm"),
                shoulder.get("Shoulderxcross"),
                chest.get("Templarchest"),
                cloak.get("Cloakoftemplar"),
                boots.get("Bootsheavy"),
                hppool[0],150)

Katarina = knight("Katarina",
                  heads.get("Templarhelm"),
                  shoulder.get("Shoulderxcross"),
                  chest.get("Templarchest"),
                  cloak.get("Cloakoftemplar"),
                  boots.get("Bootsheavy"),
                  hppool[0],150)


def main():
  choose = input("Choose 1. 2. 3.")
  if choose == "1":
    Rodriguez.introduction()
    Rodriguez.attack()

main()